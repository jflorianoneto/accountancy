import subprocess, shlex
from django.contrib.contenttypes.models import ContentType
from django.contrib import admin
from .models import FiatExport

class FiatExportAdmin(admin.ModelAdmin):
    list_display = ('process_name', 'xls_file', 'pdf_file')

    actions = ['export_pdf_xls']
    def export_pdf_xls(self, request, queryset):
        for fiat_exp in queryset:
            path_media   = '/home/jflorianoneto/project/media'
            venv_pdf_xls = '/home/jflorianoneto/venv_project/bin/python3'
            file_export  = '/home/jflorianoneto/project/accountancy/export_pdf_xls.py'
            command_line = '%s %s %s/%s %s/%s' % (venv_pdf_xls, file_export, path_media, fiat_exp.pdf_file, path_media, fiat_exp.xls_file)
            command = shlex.split(command_line)
            subprocess.run(command)

        self.short_description = "Export PDF ->  XLS"


admin.site.register(FiatExport, FiatExportAdmin)
