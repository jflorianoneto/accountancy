import sys
import pdfplumber
from unidecode import unidecode
import xlrd
from datetime import datetime
from xlutils.copy import copy

FILE_PDF=sys.argv[1]
FILE_XLS=sys.argv[2]
FILE_XLS_COPY='/home/jflorianoneto/project/media/media/fiat/copy_%s' % FILE_XLS

XLS_SHEET_FIND='Pto'
PDF_TITLE_PAGE_FIND='DEMONSTRATIVO DE FREQUENCIA - HISTORICO'

# ------------------------------------------------------------------------------------------------------- #
class XLSSheet():
    def __init__(self):
        self.xls_name = ''
        self.sheet_name =''
        self.dates = []

    def get_dates(self):
        for dat in self.dates:
            print(dat['date'])

    def get_workbook(self):
       book = xlrd.open_workbook(self.xls_name, formatting_info=True)
       return book

    def load_content_xls(self, xls_name, xls_sheet):
        book = xlrd.open_workbook(xls_name, formatting_info=True)
        this_sheet = book.sheet_by_name(xls_sheet)
        dates = []

        self.xls_name = xls_name
        self.sheet_name = xls_sheet

        print("$ Load XLS Sheet : [%s->%s]  %s rows " % (xls_name, xls_sheet, this_sheet.nrows ) )
        total_row_find = 0

        for row in range(0,this_sheet.nrows):
            cell = this_sheet.cell(row,0) # column A
            if row >= 8 and cell.value and cell.value != 'TOTAL': # start marks, no column TOTAL
                total_row_find = total_row_find + 1
                date_xls = int(cell.value)
                dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + date_xls - 2)
                self.dates.append({'column':0 , 'row':row+1, 'date':datetime.date(dt)})

        print(" - total de datas encontradas : %s " % (total_row_find) )

class PDF():
    def __init__(self):
        self.record = ''
        self.pdf_name = ''
        self.frequency = []

    def load_content_pdf(self):
        pdf_return =  self
        total_page_find = 0
        with pdfplumber.open(FILE_PDF) as pdf:
            total_pages = len(pdf.pages)
            print("$ Load PDF content - %s pages" % total_pages )
            for pages in pdf.pages:
#                print(" - %s %s" % ( (int(((pages.page_number/total_pages)*100))), '%')  , end='\r')
                if pages.page_number in range(135,300):   #total_pages):
                    if PDF_TITLE_PAGE_FIND in unidecode(pages.extract_text()):
                        table = pages.extract_table()
                        record = table[1][0].split('\n')[0]
                        name = table[1][0].split('\n')[1]
                        pdf_return.name = name
                        pdf_return.record = record
                        workload = []
                        if len(table) > 2:
                            list_freq = table[2]
                            for t in list_freq:
                                workload.append(unidecode(t).replace('Horarios: ',''))

                        if name and record:
                            total_page_find = total_page_find + 1
                            for line in table:
                                if 'DATA' in line[0]:
                                    for l in line[0].split('\n')[1:]:
                                        data = l.split(' ')[0]
                                        semana = l.split(' ')[1]
                                        horarios = l.split(' ')[2]
                                        marcacoes = l.lstrip().rstrip().split(' ')[3:]
                                        pdf_return.frequency.append({'date':data, 'week':semana, 'times':horarios, 'marks':marcacoes, 'page':pages.page_number, 'workload':workload})
            pdf.close()
            print(" - %s : %s pages" % (PDF_TITLE_PAGE_FIND, total_page_find) )

        self = pdf_return

    def compare_frequency(self, date_object):
        date_compare = date_object['date']
        freq_return = ''
        for freq in self.frequency:
            date_freq = datetime.date(datetime.strptime(freq['date'],'%d/%m/%Y'))
            if date_freq == date_compare:
                freq_return = freq

        return freq_return

# ------------------------------------------------------------------------------------------------------- #
def validate_marks(xls, pdf, xls_copy):
    print("$ Validate Marks - PDF x XLS")
    for i, dat in enumerate(xls.dates):
        add_frequency = pdf.compare_frequency(dat)
 #       print(" - %s %s" % ( int(((i/len(xls.dates))*100 )), '%')  , end='\r')
        if add_frequency:
            for w in add_frequency['workload']:
                time_workload = datetime.strptime(w.split(' ')[1], '%H:%M').time()
                date_row = dat['row'] - 1
                try:
                    time_frequency =  datetime.strptime(add_frequency['marks'][0], '%H:%M').time()
                    in_freq = add_frequency['marks'][0]
                    out_freq = add_frequency['marks'][1]

                    if (in_freq) < '12:00': # (w.split(' ')[1]):
                        in_column = 3
                        out_column = 5
                    else:
                        in_column = 6
                        out_column = 7

                    sheet_copy = xls_copy.get_sheet(1)
                    sheet_copy.write(date_row, in_column, in_freq)
                    sheet_copy.write(date_row, out_column, out_freq)
                    xls_copy.save(FILE_XLS_COPY)

                except:
                    pass

# ------------------------------------------------------------------------------------------------------- #
def main():
    xls = XLSSheet()
    pdf = PDF()

    xls.load_content_xls(FILE_XLS, XLS_SHEET_FIND)
    pdf.load_content_pdf()
    xls_copy = copy(xls.get_workbook())

    validate_marks(xls, pdf, xls_copy)

# ------------------------------------------------------------------------------------------------------- #
main()
