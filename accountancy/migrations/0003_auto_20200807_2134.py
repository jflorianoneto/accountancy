# Generated by Django 2.2.7 on 2020-08-08 00:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountancy', '0002_auto_20200807_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fiat_export_pdf',
            name='pdf_file',
            field=models.FileField(upload_to='media/fiat'),
        ),
        migrations.AlterField(
            model_name='fiat_export_pdf',
            name='xls_file',
            field=models.FileField(upload_to='media/fiat'),
        ),
    ]
