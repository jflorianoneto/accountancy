from django.db import models

# Create your models here.
class FiatExport(models.Model):
    process_name = models.CharField(max_length=255)
    pdf_file = models.FileField(upload_to='media/fiat')
    xls_file = models.FileField(upload_to='media/fiat')
    pub_date = models.DateTimeField('date published')
