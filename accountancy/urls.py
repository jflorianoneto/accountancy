from django.urls import path

from . import views

urlpatterns = [
    path('fiat/', views.fiat, name='fiat'),
]
