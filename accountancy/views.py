from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage

def fiat(request):
    if request.method == 'POST' and request.FILES['pdf_file'] and request.FILES['xls_file']:
        pdf_file = request.FILES['pdf_file']
        xls_file = request.FILES['xls_file']
        fs = FileSystemStorage()

        pdf_filename = fs.save(pdf_file.name, pdf_file)
        pdf_uploaded_file_url = fs.url(pdf_filename)

        xls_filename = fs.save(xls_file.name, xls_file)
        xls_uploaded_file_url = fs.url(xls_filename)
        return render(request, 'accountancy/fiat.html', {
            'pdf_uploaded_file_url': pdf_uploaded_file_url,
            'xls_uploaded_file_url': xls_uploaded_file_url
        })
    return render(request, 'accountancy/fiat.html')
