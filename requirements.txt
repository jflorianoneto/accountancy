-f /usr/share/pip-wheels
Django==1.11.26
chardet==3.0.4
pdfminer.six==20200104
pdfplumber==0.5.22
Pillow==7.2.0
pycryptodome==3.9.8
pytz==2020.1
sortedcontainers==2.2.2
sqlparse==0.3.1
unicodecsv==0.14.1
Unidecode==1.1.1
Wand==0.6.2
xlrd==1.2.0
xlutils==2.0.0
xlwt==1.3.0
